From 520f58d3243121b1f923be4e783947dca131b286 Mon Sep 17 00:00:00 2001
From: Chris Adams <chris.adams@qinetic.com.au>
Date: Thu, 29 Oct 2020 11:33:56 +1000
Subject: [PATCH 21/32] Update qHash methods to return size_t instead of uint

Change-Id: Ic2166212908b7f2e2033e80fbe9e9ce1633d164e
Reviewed-by: Bea Lam <bea.lam@jollamobile.com>
Signed-off-by: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
---
 src/contacts/qcontact.cpp                     |  4 ++--
 src/contacts/qcontact.h                       |  2 +-
 src/contacts/qcontactactiondescriptor.cpp     |  4 ++--
 src/contacts/qcontactactiondescriptor.h       |  4 ++--
 src/contacts/qcontactactiontarget.cpp         |  4 ++--
 src/contacts/qcontactactiontarget.h           |  2 +-
 src/contacts/qcontactcollection.cpp           |  4 ++--
 src/contacts/qcontactcollection.h             |  4 ++--
 src/contacts/qcontactcollectionid.cpp         |  2 +-
 src/contacts/qcontactcollectionid.h           |  2 +-
 src/contacts/qcontactdetail.cpp               |  4 ++--
 src/contacts/qcontactdetail.h                 |  2 +-
 src/contacts/qcontactid.cpp                   |  2 +-
 src/contacts/qcontactid.h                     |  2 +-
 src/contacts/qcontactrelationship.cpp         |  2 +-
 src/contacts/qcontactrelationship.h           |  2 +-
 src/organizer/qorganizercollection.cpp        |  4 ++--
 src/organizer/qorganizercollection.h          |  4 ++--
 src/organizer/qorganizercollectionid.cpp      |  2 +-
 src/organizer/qorganizercollectionid.h        |  2 +-
 src/organizer/qorganizeritem.cpp              |  4 ++--
 src/organizer/qorganizeritem.h                |  2 +-
 src/organizer/qorganizeritemdetail.cpp        |  4 ++--
 src/organizer/qorganizeritemdetail.h          |  4 ++--
 src/organizer/qorganizeritemid.cpp            |  2 +-
 src/organizer/qorganizeritemid.h              |  2 +-
 src/organizer/qorganizerrecurrencerule.cpp    | 20 +++++++++----------
 src/organizer/qorganizerrecurrencerule.h      |  4 ++--
 src/versit/qversitdocument.cpp                |  4 ++--
 src/versit/qversitdocument.h                  |  2 +-
 src/versit/qversitproperty.cpp                |  4 ++--
 src/versit/qversitproperty.h                  |  2 +-
 .../qcontactmanager/tst_qcontactmanager.cpp   |  2 +-
 33 files changed, 57 insertions(+), 57 deletions(-)

diff --git a/src/contacts/qcontact.cpp b/src/contacts/qcontact.cpp
index 9554a00f..481463ef 100644
--- a/src/contacts/qcontact.cpp
+++ b/src/contacts/qcontact.cpp
@@ -535,9 +535,9 @@ bool QContact::operator==(const QContact& other) const
     \relates QContact
     Returns the hash value for \a key.
 */
-uint qHash(const QContact &key)
+size_t qHash(const QContact &key)
 {
-    uint hash = qHash(key.id());
+    size_t hash = qHash(key.id());
     foreach (const QContactDetail& detail, key.details()) {
         hash += qHash(detail);
     }
diff --git a/src/contacts/qcontact.h b/src/contacts/qcontact.h
index 9327fd5d..35a1079c 100644
--- a/src/contacts/qcontact.h
+++ b/src/contacts/qcontact.h
@@ -137,7 +137,7 @@ private:
     QSharedDataPointer<QContactData> d;
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContact& key);
+Q_CONTACTS_EXPORT size_t qHash(const QContact& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_CONTACTS_EXPORT QDebug operator<<(QDebug dbg, const QContact& contact);
 #endif
diff --git a/src/contacts/qcontactactiondescriptor.cpp b/src/contacts/qcontactactiondescriptor.cpp
index 1866e30d..a88b9a60 100644
--- a/src/contacts/qcontactactiondescriptor.cpp
+++ b/src/contacts/qcontactactiondescriptor.cpp
@@ -303,9 +303,9 @@ bool QContactActionDescriptor::operator!=(const QContactActionDescriptor& other)
 }
 
 /*! Returns the hash value for \a key. */
-uint qHash(const QContactActionDescriptor& key)
+size_t qHash(const QContactActionDescriptor& key)
 {
-    uint ret = 0;
+    size_t ret = 0;
 
     ret += QT_PREPEND_NAMESPACE(qHash)(key.serviceName())
             + QT_PREPEND_NAMESPACE(qHash)(key.actionName())
diff --git a/src/contacts/qcontactactiondescriptor.h b/src/contacts/qcontactactiondescriptor.h
index 4aa63a45..541c4487 100644
--- a/src/contacts/qcontactactiondescriptor.h
+++ b/src/contacts/qcontactactiondescriptor.h
@@ -98,10 +98,10 @@ private:
 #ifndef QT_NO_DEBUG_STREAM
     Q_CONTACTS_EXPORT friend QDebug& operator<<(QDebug dbg, const QContactActionDescriptor& descriptor);
 #endif
-    Q_CONTACTS_EXPORT friend uint qHash(const QContactActionDescriptor& key);
+    Q_CONTACTS_EXPORT friend size_t qHash(const QContactActionDescriptor& key);
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContactActionDescriptor& key);
+Q_CONTACTS_EXPORT size_t qHash(const QContactActionDescriptor& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_CONTACTS_EXPORT QDebug& operator<<(QDebug dbg, const QContactActionDescriptor& descriptor);
 #endif
diff --git a/src/contacts/qcontactactiontarget.cpp b/src/contacts/qcontactactiontarget.cpp
index 6340e1f9..4fe41a54 100644
--- a/src/contacts/qcontactactiontarget.cpp
+++ b/src/contacts/qcontactactiontarget.cpp
@@ -194,9 +194,9 @@ QContactActionTarget::Type QContactActionTarget::type() const
 }
 
 /*! Returns the hash value for \a key. */
-uint qHash(const QContactActionTarget& key)
+size_t qHash(const QContactActionTarget& key)
 {
-    uint ret = qHash(key.contact());
+    size_t ret = qHash(key.contact());
     foreach (const QContactDetail& det, key.details()) {
         ret += qHash(det);
     }
diff --git a/src/contacts/qcontactactiontarget.h b/src/contacts/qcontactactiontarget.h
index 706dce18..000d30e4 100644
--- a/src/contacts/qcontactactiontarget.h
+++ b/src/contacts/qcontactactiontarget.h
@@ -88,7 +88,7 @@ private:
     QSharedDataPointer<QContactActionTargetPrivate> d;
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContactActionTarget& key);
+Q_CONTACTS_EXPORT size_t qHash(const QContactActionTarget& key);
 #ifndef QT_NO_DATASTREAM
 Q_CONTACTS_EXPORT QDataStream& operator<<(QDataStream& out, const QContactActionTarget& target);
 Q_CONTACTS_EXPORT QDataStream& operator>>(QDataStream& in, QContactActionTarget& target);
diff --git a/src/contacts/qcontactcollection.cpp b/src/contacts/qcontactcollection.cpp
index a07b48c6..da94a58b 100644
--- a/src/contacts/qcontactcollection.cpp
+++ b/src/contacts/qcontactcollection.cpp
@@ -221,9 +221,9 @@ QVariant QContactCollection::metaData(MetaDataKey key) const
     \relates QContactCollection
     Returns the hash value for \a key.
  */
-Q_CONTACTS_EXPORT uint qHash(const QContactCollection &key)
+Q_CONTACTS_EXPORT size_t qHash(const QContactCollection &key)
 {
-    uint hash = qHash(key.id());
+    size_t hash = qHash(key.id());
     QMap<QContactCollection::MetaDataKey, QVariant>::const_iterator i = key.d->m_metaData.constBegin();
     while (i != key.d->m_metaData.constEnd()) {
         if (i.key() == QContactCollection::KeyExtended) {
diff --git a/src/contacts/qcontactcollection.h b/src/contacts/qcontactcollection.h
index f04b0618..5a0fd234 100644
--- a/src/contacts/qcontactcollection.h
+++ b/src/contacts/qcontactcollection.h
@@ -89,12 +89,12 @@ public:
     QVariant extendedMetaData(const QString &key) const;
 
 private:
-    friend Q_CONTACTS_EXPORT uint qHash(const QContactCollection &key);
+    friend Q_CONTACTS_EXPORT size_t qHash(const QContactCollection &key);
     friend class QContactManagerEngine;
     QSharedDataPointer<QContactCollectionData> d;
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContactCollection &key);
+Q_CONTACTS_EXPORT size_t qHash(const QContactCollection &key);
 
 #ifndef QT_NO_DEBUG_STREAM
 Q_CONTACTS_EXPORT QDebug operator<<(QDebug dbg, const QContactCollection &collection);
diff --git a/src/contacts/qcontactcollectionid.cpp b/src/contacts/qcontactcollectionid.cpp
index 0665cb3d..9a6a31ea 100644
--- a/src/contacts/qcontactcollectionid.cpp
+++ b/src/contacts/qcontactcollectionid.cpp
@@ -110,7 +110,7 @@ QT_BEGIN_NAMESPACE_CONTACTS
 */
 
 /*!
-    \fn uint qHash(const QContactCollectionId &id)
+    \fn size_t qHash(const QContactCollectionId &id)
     \relates QContactCollectionId
 
     Returns the hash value for \a id.
diff --git a/src/contacts/qcontactcollectionid.h b/src/contacts/qcontactcollectionid.h
index d0e52d0f..e8106a37 100644
--- a/src/contacts/qcontactcollectionid.h
+++ b/src/contacts/qcontactcollectionid.h
@@ -83,7 +83,7 @@ private:
 inline bool operator<(const QContactCollectionId &id1, const QContactCollectionId &id2)
 { return id1.managerUri() != id2.managerUri() ? id1.managerUri() < id2.managerUri() : id1.localId() < id2.localId(); }
 
-inline uint qHash(const QContactCollectionId &id)
+inline size_t qHash(const QContactCollectionId &id)
 { return qHash(id.localId()); }
 
 #ifndef QT_NO_DATASTREAM
diff --git a/src/contacts/qcontactdetail.cpp b/src/contacts/qcontactdetail.cpp
index 8671484a..e665826d 100644
--- a/src/contacts/qcontactdetail.cpp
+++ b/src/contacts/qcontactdetail.cpp
@@ -286,9 +286,9 @@ bool QContactDetail::operator==(const QContactDetail& other) const
 
 /*! Returns the hash value for \a key.
 */
-uint qHash(const QContactDetail &key)
+size_t qHash(const QContactDetail &key)
 {
-    uint hash = qHash(QString().setNum(key.type()))
+    size_t hash = qHash(QString().setNum(key.type()))
                 + QT_PREPEND_NAMESPACE(qHash)(key.accessConstraints());
     QMap<int, QVariant> values = key.values();
     QMap<int, QVariant>::const_iterator it = values.constBegin(), end = values.constEnd();
diff --git a/src/contacts/qcontactdetail.h b/src/contacts/qcontactdetail.h
index f3ccef6c..9ad3d2d8 100644
--- a/src/contacts/qcontactdetail.h
+++ b/src/contacts/qcontactdetail.h
@@ -200,7 +200,7 @@ private:
 #endif
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContactDetail& key);
+Q_CONTACTS_EXPORT size_t qHash(const QContactDetail& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_CONTACTS_EXPORT QDebug operator<<(QDebug dbg, const QContactDetail& detail);
 #endif
diff --git a/src/contacts/qcontactid.cpp b/src/contacts/qcontactid.cpp
index c0ef98cc..e06bd795 100644
--- a/src/contacts/qcontactid.cpp
+++ b/src/contacts/qcontactid.cpp
@@ -109,7 +109,7 @@ QT_BEGIN_NAMESPACE_CONTACTS
 */
 
 /*!
-    \fn uint qHash(const QContactId &id)
+    \fn size_t qHash(const QContactId &id)
     \relates QContactId
 
     Returns the hash value for \a id.
diff --git a/src/contacts/qcontactid.h b/src/contacts/qcontactid.h
index f6008004..ecb97bd5 100644
--- a/src/contacts/qcontactid.h
+++ b/src/contacts/qcontactid.h
@@ -82,7 +82,7 @@ private:
 inline bool operator<(const QContactId &id1, const QContactId &id2)
 { return id1.managerUri() != id2.managerUri() ? id1.managerUri() < id2.managerUri() : id1.localId() < id2.localId(); }
 
-inline uint qHash(const QContactId &id)
+inline size_t qHash(const QContactId &id)
 { return qHash(id.localId()); }
 
 #ifndef QT_NO_DEBUG_STREAM
diff --git a/src/contacts/qcontactrelationship.cpp b/src/contacts/qcontactrelationship.cpp
index 81fca56d..b070ad8e 100644
--- a/src/contacts/qcontactrelationship.cpp
+++ b/src/contacts/qcontactrelationship.cpp
@@ -161,7 +161,7 @@ bool QContactRelationship::operator==(const QContactRelationship &other) const
 /*!
  * Returns the hash value for \a key.
  */
-uint qHash(const QContactRelationship &key)
+size_t qHash(const QContactRelationship &key)
 {
     return qHash(key.first()) + qHash(key.second())
         + QT_PREPEND_NAMESPACE(qHash)(key.relationshipType());
diff --git a/src/contacts/qcontactrelationship.h b/src/contacts/qcontactrelationship.h
index a1038e1a..2256e99f 100644
--- a/src/contacts/qcontactrelationship.h
+++ b/src/contacts/qcontactrelationship.h
@@ -86,7 +86,7 @@ private:
     QSharedDataPointer<QContactRelationshipPrivate> d;
 };
 
-Q_CONTACTS_EXPORT uint qHash(const QContactRelationship& key);
+Q_CONTACTS_EXPORT size_t qHash(const QContactRelationship& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_CONTACTS_EXPORT QDebug operator<<(QDebug dbg, const QContactRelationship& rel);
 #endif
diff --git a/src/organizer/qorganizercollection.cpp b/src/organizer/qorganizercollection.cpp
index 21d83e45..96d8605e 100644
--- a/src/organizer/qorganizercollection.cpp
+++ b/src/organizer/qorganizercollection.cpp
@@ -220,9 +220,9 @@ QVariant QOrganizerCollection::metaData(MetaDataKey key) const
     \relates QOrganizerCollection
     Returns the hash value for \a key.
  */
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerCollection &key)
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerCollection &key)
 {
-    uint hash = qHash(key.id());
+    size_t hash = qHash(key.id());
     QMap<QOrganizerCollection::MetaDataKey, QVariant>::const_iterator i = key.d->m_metaData.constBegin();
     while (i != key.d->m_metaData.constEnd()) {
         if (i.key() == QOrganizerCollection::KeyExtended) {
diff --git a/src/organizer/qorganizercollection.h b/src/organizer/qorganizercollection.h
index 15e44f85..b9a077de 100644
--- a/src/organizer/qorganizercollection.h
+++ b/src/organizer/qorganizercollection.h
@@ -88,12 +88,12 @@ public:
     QVariant extendedMetaData(const QString &key) const;
 
 private:
-    friend Q_ORGANIZER_EXPORT uint qHash(const QOrganizerCollection &key);
+    friend Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerCollection &key);
     friend class QOrganizerManagerEngine;
     QSharedDataPointer<QOrganizerCollectionData> d;
 };
 
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerCollection &key);
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerCollection &key);
 
 #ifndef QT_NO_DEBUG_STREAM
 Q_ORGANIZER_EXPORT QDebug operator<<(QDebug dbg, const QOrganizerCollection &collection);
diff --git a/src/organizer/qorganizercollectionid.cpp b/src/organizer/qorganizercollectionid.cpp
index e3da3851..d55def8b 100644
--- a/src/organizer/qorganizercollectionid.cpp
+++ b/src/organizer/qorganizercollectionid.cpp
@@ -109,7 +109,7 @@ QT_BEGIN_NAMESPACE_ORGANIZER
 */
 
 /*!
-    \fn uint qHash(const QOrganizerCollectionId &id)
+    \fn size_t qHash(const QOrganizerCollectionId &id)
     \relates QOrganizerCollectionId
 
     Returns the hash value for \a id.
diff --git a/src/organizer/qorganizercollectionid.h b/src/organizer/qorganizercollectionid.h
index cbd27203..08eea000 100644
--- a/src/organizer/qorganizercollectionid.h
+++ b/src/organizer/qorganizercollectionid.h
@@ -82,7 +82,7 @@ private:
 inline bool operator<(const QOrganizerCollectionId &id1, const QOrganizerCollectionId &id2)
 { return id1.managerUri() != id2.managerUri() ? id1.managerUri() < id2.managerUri() : id1.localId() < id2.localId(); }
 
-inline uint qHash(const QOrganizerCollectionId &id)
+inline size_t qHash(const QOrganizerCollectionId &id)
 { return qHash(id.localId()); }
 
 #ifndef QT_NO_DATASTREAM
diff --git a/src/organizer/qorganizeritem.cpp b/src/organizer/qorganizeritem.cpp
index 8e4cc863..8f5e76f0 100644
--- a/src/organizer/qorganizeritem.cpp
+++ b/src/organizer/qorganizeritem.cpp
@@ -450,9 +450,9 @@ bool QOrganizerItem::operator==(const QOrganizerItem &other) const
 
     Returns the hash value for \a key.
  */
-uint qHash(const QOrganizerItem &key)
+size_t qHash(const QOrganizerItem &key)
 {
-    uint hash = qHash(key.id());
+    size_t hash = qHash(key.id());
     hash += qHash(key.collectionId());
     foreach (const QOrganizerItemDetail &detail, key.details())
         hash += qHash(detail);
diff --git a/src/organizer/qorganizeritem.h b/src/organizer/qorganizeritem.h
index cd6d4165..89a6a493 100644
--- a/src/organizer/qorganizeritem.h
+++ b/src/organizer/qorganizeritem.h
@@ -123,7 +123,7 @@ protected:
     QSharedDataPointer<QOrganizerItemData> d;
 };
 
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerItem &key);
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerItem &key);
 
 #ifndef QT_NO_DATASTREAM
 Q_ORGANIZER_EXPORT QDataStream &operator<<(QDataStream &out, const QOrganizerItem &item);
diff --git a/src/organizer/qorganizeritemdetail.cpp b/src/organizer/qorganizeritemdetail.cpp
index 431a59b0..c2ff7f4a 100644
--- a/src/organizer/qorganizeritemdetail.cpp
+++ b/src/organizer/qorganizeritemdetail.cpp
@@ -255,9 +255,9 @@ bool QOrganizerItemDetail::operator==(const QOrganizerItemDetail &other) const
     \relates QOrganizerItemDetail
     Returns the hash value for \a key.
  */
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerItemDetail &key)
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerItemDetail &key)
 {
-    uint hash = QT_PREPEND_NAMESPACE(qHash)(key.d->m_detailType);
+    size_t hash = QT_PREPEND_NAMESPACE(qHash)(key.d->m_detailType);
     QMap<int, QVariant>::const_iterator it = key.d->m_values.constBegin();
     while (it != key.d->m_values.constEnd()) {
         hash += QT_PREPEND_NAMESPACE(qHash)(it.key()) + QT_PREPEND_NAMESPACE(qHash)(it.value().toString());
diff --git a/src/organizer/qorganizeritemdetail.h b/src/organizer/qorganizeritemdetail.h
index 0c4f1392..fad8932b 100644
--- a/src/organizer/qorganizeritemdetail.h
+++ b/src/organizer/qorganizeritemdetail.h
@@ -112,7 +112,7 @@ protected:
     QOrganizerItemDetail &assign(const QOrganizerItemDetail &other, DetailType expectedDetailType);
 
 private:
-    friend Q_ORGANIZER_EXPORT uint qHash(const QOrganizerItemDetail &key);
+    friend Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerItemDetail &key);
     friend class QOrganizerItem;
     friend class QOrganizerItemDetailPrivate;
 
@@ -123,7 +123,7 @@ private:
     QSharedDataPointer<QOrganizerItemDetailPrivate> d;
 };
 
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerItemDetail &key);
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerItemDetail &key);
 #ifndef QT_NO_DATASTREAM
 Q_ORGANIZER_EXPORT QDataStream &operator<<(QDataStream &out, const QOrganizerItemDetail &detail);
 Q_ORGANIZER_EXPORT QDataStream &operator>>(QDataStream &in, QOrganizerItemDetail &detail);
diff --git a/src/organizer/qorganizeritemid.cpp b/src/organizer/qorganizeritemid.cpp
index 4d2fa71b..2c1c2a0c 100644
--- a/src/organizer/qorganizeritemid.cpp
+++ b/src/organizer/qorganizeritemid.cpp
@@ -109,7 +109,7 @@ QT_BEGIN_NAMESPACE_ORGANIZER
 */
 
 /*!
-    \fn uint qHash(const QOrganizerItemId &id)
+    \fn size_t qHash(const QOrganizerItemId &id)
     \relates QOrganizerItemId
 
     Returns the hash value for \a id.
diff --git a/src/organizer/qorganizeritemid.h b/src/organizer/qorganizeritemid.h
index bdca8f8d..39d5df98 100644
--- a/src/organizer/qorganizeritemid.h
+++ b/src/organizer/qorganizeritemid.h
@@ -82,7 +82,7 @@ private:
 inline bool operator<(const QOrganizerItemId &id1, const QOrganizerItemId &id2)
 { return id1.managerUri() != id2.managerUri() ? id1.managerUri() < id2.managerUri() : id1.localId() < id2.localId(); }
 
-inline uint qHash(const QOrganizerItemId &id)
+inline size_t qHash(const QOrganizerItemId &id)
 { return qHash(id.localId()); }
 
 #ifndef QT_NO_DATASTREAM
diff --git a/src/organizer/qorganizerrecurrencerule.cpp b/src/organizer/qorganizerrecurrencerule.cpp
index c5c30e57..c67fe6f4 100644
--- a/src/organizer/qorganizerrecurrencerule.cpp
+++ b/src/organizer/qorganizerrecurrencerule.cpp
@@ -485,12 +485,12 @@ Qt::DayOfWeek QOrganizerRecurrenceRule::firstDayOfWeek() const
     \relates QOrganizerRecurrenceRule
     Returns the hash value for \a key.
  */
-uint qHash(const QOrganizerRecurrenceRule &key)
+size_t qHash(const QOrganizerRecurrenceRule &key)
 {
-    uint hash(0);
-    static const unsigned int prime1 = 11;
-    static const unsigned int prime2 = 31;
-    static const unsigned int prime3 = 47;
+    size_t hash(0);
+    static const size_t prime1 = 11;
+    static const size_t prime2 = 31;
+    static const size_t prime3 = 47;
 
     foreach (int day, key.daysOfMonth())
         hash += day;
@@ -516,12 +516,12 @@ uint qHash(const QOrganizerRecurrenceRule &key)
         hash += pos;
     hash *= prime3;
 
-    hash += static_cast<uint>(key.firstDayOfWeek())
-            + static_cast<uint>(key.frequency())
-            + key.interval()
-            + key.limitCount()
+    hash += static_cast<size_t>(key.firstDayOfWeek())
+            + static_cast<size_t>(key.frequency())
+            + static_cast<size_t>(key.interval())
+            + static_cast<size_t>(key.limitCount())
             + qHash(key.limitDate())
-            + static_cast<uint>(key.limitType());
+            + static_cast<size_t>(key.limitType());
 
     return hash * prime1;
 }
diff --git a/src/organizer/qorganizerrecurrencerule.h b/src/organizer/qorganizerrecurrencerule.h
index 2e36b73d..c80ce70f 100644
--- a/src/organizer/qorganizerrecurrencerule.h
+++ b/src/organizer/qorganizerrecurrencerule.h
@@ -131,13 +131,13 @@ private:
 };
 
 //hash functions
-Q_ORGANIZER_EXPORT uint qHash(const QOrganizerRecurrenceRule &rule);
+Q_ORGANIZER_EXPORT size_t qHash(const QOrganizerRecurrenceRule &rule);
 
 #ifndef QT_NO_DEBUG_STREAM
 Q_ORGANIZER_EXPORT QDebug operator<<(QDebug dbg, const QOrganizerRecurrenceRule &rule);
 #endif // QT_NO_DEBUG_STREAM
 
-inline uint qHash(QOrganizerRecurrenceRule::Month month)
+inline size_t qHash(QOrganizerRecurrenceRule::Month month)
 {
     return static_cast<uint>(month);
 }
diff --git a/src/versit/qversitdocument.cpp b/src/versit/qversitdocument.cpp
index 5b7a91f3..3ab69518 100644
--- a/src/versit/qversitdocument.cpp
+++ b/src/versit/qversitdocument.cpp
@@ -145,9 +145,9 @@ bool QVersitDocument::operator!=(const QVersitDocument& other) const
 }
 
 /*! Returns the hash value for \a key. */
-uint qHash(const QVersitDocument &key)
+size_t qHash(const QVersitDocument &key)
 {
-    int hash = QT_PREPEND_NAMESPACE(qHash)(key.type());
+    size_t hash = QT_PREPEND_NAMESPACE(qHash)(key.type());
     hash += QT_PREPEND_NAMESPACE(qHash)(key.componentType());
     hash += key.properties().length() + key.subDocuments().length();
     foreach (const QVersitProperty& property, key.properties()) {
diff --git a/src/versit/qversitdocument.h b/src/versit/qversitdocument.h
index 5f9cb570..22faebc7 100644
--- a/src/versit/qversitdocument.h
+++ b/src/versit/qversitdocument.h
@@ -100,7 +100,7 @@ private:
     QSharedDataPointer<QVersitDocumentPrivate> d;
 };
 
-Q_VERSIT_EXPORT uint qHash(const QVersitDocument& key);
+Q_VERSIT_EXPORT size_t qHash(const QVersitDocument& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_VERSIT_EXPORT QDebug operator<<(QDebug dbg, const QVersitDocument& property);
 #endif
diff --git a/src/versit/qversitproperty.cpp b/src/versit/qversitproperty.cpp
index b0999c01..df955a3d 100644
--- a/src/versit/qversitproperty.cpp
+++ b/src/versit/qversitproperty.cpp
@@ -156,9 +156,9 @@ bool QVersitProperty::operator!=(const QVersitProperty& other) const
 }
 
 /*! Returns the hash value for \a key. */
-uint qHash(const QVersitProperty &key)
+size_t qHash(const QVersitProperty &key)
 {
-    uint hash = QT_PREPEND_NAMESPACE(qHash)(key.name()) + QT_PREPEND_NAMESPACE(qHash)(key.value());
+    size_t hash = QT_PREPEND_NAMESPACE(qHash)(key.name()) + QT_PREPEND_NAMESPACE(qHash)(key.value());
     foreach (const QString& group, key.groups()) {
         hash += QT_PREPEND_NAMESPACE(qHash)(group);
     }
diff --git a/src/versit/qversitproperty.h b/src/versit/qversitproperty.h
index 1521e6f5..c6e1329e 100644
--- a/src/versit/qversitproperty.h
+++ b/src/versit/qversitproperty.h
@@ -102,7 +102,7 @@ private:
     QSharedDataPointer<QVersitPropertyPrivate> d;
 };
 
-Q_VERSIT_EXPORT uint qHash(const QVersitProperty& key);
+Q_VERSIT_EXPORT size_t qHash(const QVersitProperty& key);
 #ifndef QT_NO_DEBUG_STREAM
 Q_VERSIT_EXPORT QDebug operator<<(QDebug dbg, const QVersitProperty& property);
 #endif
diff --git a/tests/auto/contacts/qcontactmanager/tst_qcontactmanager.cpp b/tests/auto/contacts/qcontactmanager/tst_qcontactmanager.cpp
index a46cf97a..edd985d1 100644
--- a/tests/auto/contacts/qcontactmanager/tst_qcontactmanager.cpp
+++ b/tests/auto/contacts/qcontactmanager/tst_qcontactmanager.cpp
@@ -213,7 +213,7 @@ private:
 };
 
 QT_BEGIN_NAMESPACE
-static uint qHash(const QMetaMethod &m)
+static size_t qHash(const QMetaMethod &m)
 {
     return qHash(m.methodIndex());
 }
-- 
2.30.2

